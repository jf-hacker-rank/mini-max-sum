using System.Linq;
using MiniMaxSum;
using NUnit.Framework;

namespace MiniMaxTests;

public class Tests
{
    [SetUp]
    public void Setup()
    {
    }

    [Test]
    public void Test1()
    {
        int[] sumTest = new[] { 1, 3, 5, 7, 9 };
        string res = Result.miniMaxSum(sumTest.ToList());
        Assert.AreEqual("16 24", res);
    }

    [Test]
    public void BigIntegers()
    {
        // 2,744,467,344
        int[] sumTest = new[] { 256741038, 623958417, 467905213, 714532089, 938071625 };
        string res = Result.miniMaxSum(sumTest.ToList());
        Assert.AreEqual("2063136757 2744467344", res);
    }
    
    [Test]
    public void NegativeIntegers()
    {
        // 2,744,467,344
        int[] sumTest = new[] { -5, -4, -3, -2, -1};
        string res = Result.miniMaxSum(sumTest.ToList());
        Assert.AreEqual("-14 -10", res);
    }
}