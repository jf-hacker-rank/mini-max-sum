using System.Numerics;

namespace MiniMaxSum;

public class Result
{

    /*
     * Complete the 'miniMaxSum' function below.
     *
     * The function accepts INTEGER_ARRAY arr as parameter.
     */

    public static string miniMaxSum(List<int> arr)
    {
        arr.Sort();
        
        // Adding like this to avoid int + int overflow, could also cast the arr items to Int64
        Int64 leastTotal = 0; 
        leastTotal += arr[0];
        leastTotal += arr[1];
        leastTotal += arr[2];
        leastTotal += arr[3];
        
        Int64 mostTotal = 0;
        mostTotal += arr[4];
        mostTotal += arr[1];
        mostTotal += arr[2];
        mostTotal += arr[3];
        return $"{leastTotal} {mostTotal}";
    }

}